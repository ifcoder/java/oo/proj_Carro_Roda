
package projcarroroda;

import classes.Carro;

/**
 *
 * @author noteifet
 */
public class ProjCarroRoda {

    public static void main(String[] args) {
        Carro c;
        c = new Carro();
        c.preencher();
        
        //agora vamos imprimir o conteudo de carro e todas as rodas
        c.imprimir();
    }
    
}
