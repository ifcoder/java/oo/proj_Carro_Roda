# Projeto Carro - Roda

Este é o projeto de relacionamente entre as classes Carro e Roda, da nossa lista de exercícios de Orientação a objetos

### Relação de Agregação

- É a relação do tipo “tem um”
- Isto significa que a classe **********Carro********** tem uma classe ********Roda********

**Definição**

- *“Indica que uma classe é um contêiner ou  uma coleção de outras classes. As classes contidas não dependem do contêiner”*

![Modelo carro-roda](./miniatura.png)

- Toda relação de agregação possui consigo uma ******cardinalidade******
    - 1..*        1
- A cardinalidade representa o quantitativo desta relação entre as classes
- No exemplo acima, lemos da seguinte forma:
    - Um carro possui no mínimo uma instancia da classe roda
    - Uma roda pertence a um único carro


## Getting started
- Clone o projeto e estude em seu computador
- Faça algumas alterações e teste, assim que a gente aprende
- Finalmente, tente melhorá-lo e criar algumas funcionalidades na função main.


